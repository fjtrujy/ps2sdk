FROM registry.gitlab.com/ps2max/ps2toolchain:latest

ENV GSKIT  $PS2DEV/gsKit

COPY . /src/ps2sdk

RUN \
  cd /src && \
  apk add --no-cache git ucl gawk zip && \
  apk add --no-cache --virtual .build-deps gcc musl-dev zlib-dev ucl-dev && \
  \
  cd ps2sdk && \
    make clean all release && \
    ln -sf "$PS2SDK/ee/startup/crt0.o"  "$PS2DEV/ee/lib/gcc-lib/ee/3.2.3/crt0.o" && \
    ln -sf "$PS2SDK/ee/startup/crt0.o"  "$PS2DEV/ee/ee/lib/crt0.o" && \
    ln -sf "$PS2SDK/ee/lib/libc.a"      "$PS2DEV/ee/ee/lib/libps2sdkc.a" && \
    ln -sf "$PS2SDK/ee/lib/libkernel.a" "$PS2DEV/ee/ee/lib/libkernel.a" && \
  cd .. && \
  \
  git clone --depth 1 https://gitlab.com/ps2max/gsKit.git && \
  cd gsKit && \
    make clean all install && \
  cd .. && \
  \
  git clone --depth 1 https://gitlab.com/ps2max/isjpcm.git && \
  cd isjpcm && \
    make clean all install && \
  cd .. && \
  \
  git clone --depth 1 https://gitlab.com/ps2max/ps2sdk-ports.git && \
  cd ps2sdk-ports && \
    make all && \
  cd .. && \
  \
  git clone --depth 1 https://gitlab.com/ps2max/ps2eth.git && \
  cd ps2eth && \
    make clean all install && \
  cd .. && \
  \
  git clone --depth 1 https://github.com/ps2dev/ps2-packer && \
  cd ps2-packer && \
    make clean all install && \
  cd .. && \
  \
  apk del .build-deps && \
  rm -rf \
    /src/* \
    /tmp/*

WORKDIR /src
